# Golang之文本编码处理

## 文本字符串处理

### [从字符串中查找子串](./strings/contains/contains.go)

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	// 字符串定义
	s := "中华人民共和国今天成立了"

	// 判断字符串是否包含	=> true false
	fmt.Println(strings.Contains(s, "今天"), strings.Contains(s, "毛主席"))

	// 获取字符串中子串的索引	=> 6 [UTF-8编码一个汉字占3个字节哦]
	fmt.Println(strings.Index(s, "人民"))
	// 获取字符串中子串的索引 	=> 4 [UTF-8编码单字节字母占一个字节]
	fmt.Println(strings.Index("i love u", "ve"))
}
```

### [字符串的切割与拼接](./strings/split.and.join/split.and.join.go)

```go
package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := `起来！不愿做奴隶的人们！{zol}把我们的血肉筑成我们新的长城！{zol}中华民族到了最危险的时候，{zol}每个人被迫着发出最后的吼声。`

	// 切割字符串 => []string => slice
	sliceLyrics := strings.Split(s, "{zol}")
	// [起来！不愿做奴隶的人们！ 把我们的血肉筑成我们新的长城！ 中华民族到了最危险的时候， 每个人被迫着发出最后的吼声。]
	fmt.Println(sliceLyrics)

	// 将slice元素内容拼接为字符串
	stringLyrics := strings.Join(sliceLyrics, "")
	// 起来！不愿做奴隶的人们！把我们的血肉筑成我们新的长城！中华民族到了最危险的时候，每个人被迫着发出最后的吼声。
	fmt.Println(stringLyrics)
}
```

#### [判断字符串是否包含特定的子串前缀或后缀](./strings/hasprefix.and.hassuffix/hasprefix.and.hassuffix.go)

```go
package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := "中华人民共和国今天成立了"

	// 判断字符串是否包含特定的子串前缀
	isHasPrefix := strings.HasPrefix(s, "中华")
	// result is => true
	fmt.Println(isHasPrefix)
	// result is => false
	fmt.Println(strings.HasPrefix(s, "中国海军"))

	// 判断字符串是否包含特定的子串后缀
	isHasSuffix := strings.HasSuffix(s, "今天成立了")
	// result is => true
	fmt.Println(isHasSuffix)
	// result is => false
	fmt.Println(strings.HasSuffix(s, "不断壮大"))
}
```

### [去除字符串中的空格与回车换行](./strings/trim/trim.go)

```go
package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := " 中华人  民共和国\r\n          今天成立了 "

	// 去除字符串中的前后空格
	newString := strings.Trim(s, " ")
	// result is:
	// '中华人  民共和国
	//           今天成立了'
	fmt.Println("'" + newString + "'")

	// 去除字符串中的空格
	newString = strings.Replace(newString, " ", "", -1)
	// result is:
	// '中华人民共和国
	// 今天成立了'
	fmt.Println("'" + newString + "'")

	// 去除字符串中的「回车换行」
	newString = strings.Replace(newString, "\r\n", "", -1)
	// result is:
	// '中华人民共和国今天成立了'
	fmt.Println("'" + newString + "'")
}
```

### [字符串的替换](./strings/replace/replace.go)

```go
package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := "中华人民共和国成立了, 三军皆在成长！"

	// 字符串替换
	newString := strings.Replace(strings.Replace(s, "了", "啦", -1), "成长", "快速强大", -1)
	// 中华人民共和国成立啦, 三军皆在快速强大！
	fmt.Println(newString)
}
```

### [数字字符串与整型之间的相互转换](./strings/convert/convert.go)

```go
package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	// 定义数字类型字符串
	stringQQ := "402838678"

	// 数字字符串转换为整型
	numberQQ, _ := strconv.Atoi(stringQQ)
	// 通过反射获取到当前变量类型 => int
	fmt.Println(reflect.TypeOf(numberQQ))

	// 将整型转换为数字字符串
	stringQQ = strconv.Itoa(numberQQ)
	// 通过反射获取到当前变量类型 => string
	fmt.Println(reflect.TypeOf(stringQQ))
}
```

### [解析字符串其它数据类型字面量到其它数据类型](./strings/parse/parse.go)

```go
package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	// 定义字符串类型的值为布尔类型的字面量变量
	stringBoolFalseLiteral := "false"
	stringBool1Literal := "1"
	// 定义字符串类型的值为浮点类型的字面量变量
	stringFloatNumLiteral := "2018.99"

	
	// 解析字符串类型值为布尔类型字面量到布尔类型
	boolLiteralOfFalse, _ := strconv.ParseBool(stringBoolFalseLiteral)
	boolLiteralOf1, _ := strconv.ParseBool(stringBool1Literal)

	// 通过反射获取当前变量类型 => bool
	// typeof is: bool, value is: false
	fmt.Printf("typeof is: %s, value is: %t\r\n", reflect.TypeOf(boolLiteralOfFalse), boolLiteralOfFalse)
	// typeof is: bool, value is: true
	fmt.Printf("typeof is: %s, value is: %t\r\n", reflect.TypeOf(boolLiteralOf1), boolLiteralOf1)

	
	// 解析字符串类型值为浮点类型字面量到浮点类型
	floatLiteralOfString, _ := strconv.ParseFloat(stringFloatNumLiteral, 64)

	// 通过反射获取当前变量类型 => float64
	// typeof is: float64, value is: 2018.990000
	fmt.Printf("typeof is: %s, value is: %f\r\n", reflect.TypeOf(floatLiteralOfString), floatLiteralOfString)
	
	// 关于字符串数字字面量解析到整型有以下方法可用
	// strconv.ParseInt()
	// strconv.ParseUint()
}
```

### 格式化其它数据类型到字符串

```go
package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	// 变量定义
	var isOkOfBool bool = true
	var numberQQOfInt int64 = 402838678
	var numberPriceOfFloat float64 = 98738338.98783833822334

	// 格式化布尔类型变量到字符串
	isOkOfString := strconv.FormatBool(isOkOfBool)
	// 通过反射获取当前变量类型 => string
	// typeof is: string, value is: true
	fmt.Printf("typeof is: %s, value is: %s\r\n", reflect.TypeOf(isOkOfString), isOkOfString)

	// 格式化整型变量到字符串
	numberQQOfString := strconv.FormatInt(numberQQOfInt, 16) // 转换十进制数字 `402838678` 到十六进制字符串 `1802d496`
	// 通过反射获取当前变量类型 => string
	// typeof is: string, value is: 1802d496
	fmt.Printf("typeof is: %s, value is: %s\r\n", reflect.TypeOf(numberQQOfString), numberQQOfString)

	// 格式化浮动型变量到字符串
	numberPriceOfString := strconv.FormatFloat(numberPriceOfFloat, 'G', 2, 64)
	// 通过反射获取当前变量类型 => string
	// typeof is: string, value is: 9.9E+07
	fmt.Printf("typeof is: %s, value is: %s\r\n", reflect.TypeOf(numberPriceOfString), numberPriceOfString)
}
```
