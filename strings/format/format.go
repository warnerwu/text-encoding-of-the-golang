package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	// 变量定义
	var isOkOfBool bool = true
	var numberQQOfInt int64 = 402838678
	var numberPriceOfFloat float64 = 98738338.98783833822334

	// 格式化布尔类型变量到字符串
	isOkOfString := strconv.FormatBool(isOkOfBool)
	// 通过反射获取当前变量类型 => string
	// typeof is: string, value is: true
	fmt.Printf("typeof is: %s, value is: %s\r\n", reflect.TypeOf(isOkOfString), isOkOfString)

	// 格式化整型变量到字符串
	numberQQOfString := strconv.FormatInt(numberQQOfInt, 16) // 转换十进制数字 `402838678` 到十六进制字符串 `1802d496`
	// 通过反射获取当前变量类型 => string
	// typeof is: string, value is: 1802d496
	fmt.Printf("typeof is: %s, value is: %s\r\n", reflect.TypeOf(numberQQOfString), numberQQOfString)

	// 格式化浮动型变量到字符串
	numberPriceOfString := strconv.FormatFloat(numberPriceOfFloat, 'G', 2, 64)
	// 通过反射获取当前变量类型 => string
	// typeof is: string, value is: 9.9E+07
	fmt.Printf("typeof is: %s, value is: %s\r\n", reflect.TypeOf(numberPriceOfString), numberPriceOfString)
}
