package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := `起来！不愿做奴隶的人们！{zol}把我们的血肉筑成我们新的长城！{zol}中华民族到了最危险的时候，{zol}每个人被迫着发出最后的吼声。`

	// 切割字符串 => []string => slice
	sliceLyrics := strings.Split(s, "{zol}")
	// [起来！不愿做奴隶的人们！ 把我们的血肉筑成我们新的长城！ 中华民族到了最危险的时候， 每个人被迫着发出最后的吼声。]
	fmt.Println(sliceLyrics)

	// 将slice元素内容拼接为字符串
	stringLyrics := strings.Join(sliceLyrics, "")
	// 起来！不愿做奴隶的人们！把我们的血肉筑成我们新的长城！中华民族到了最危险的时候，每个人被迫着发出最后的吼声。
	fmt.Println(stringLyrics)
}
