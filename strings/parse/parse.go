package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	// 定义字符串类型的值为布尔类型的字面量变量
	stringBoolFalseLiteral := "false"
	stringBool1Literal := "1"
	// 定义字符串类型的值为浮点类型的字面量变量
	stringFloatNumLiteral := "2018.99"

	// 解析字符串类型值为布尔类型字面量到布尔类型
	boolLiteralOfFalse, _ := strconv.ParseBool(stringBoolFalseLiteral)
	boolLiteralOf1, _ := strconv.ParseBool(stringBool1Literal)

	// 通过反射获取当前变量类型 => bool
	// typeof is: bool, value is: false
	fmt.Printf("typeof is: %s, value is: %t\r\n", reflect.TypeOf(boolLiteralOfFalse), boolLiteralOfFalse)
	// typeof is: bool, value is: true
	fmt.Printf("typeof is: %s, value is: %t\r\n", reflect.TypeOf(boolLiteralOf1), boolLiteralOf1)

	// 解析字符串类型值为浮点类型字面量到浮点类型
	floatLiteralOfString, _ := strconv.ParseFloat(stringFloatNumLiteral, 64)

	// 通过反射获取当前变量类型 => float64
	// typeof is: float64, value is: 2018.990000
	fmt.Printf("typeof is: %s, value is: %f\r\n", reflect.TypeOf(floatLiteralOfString), floatLiteralOfString)

	// 关于字符串数字字面量解析到整型有以下方法可用
	// strconv.ParseInt()
	// strconv.ParseUint()
}
