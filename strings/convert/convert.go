package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	// 定义数字类型字符串
	stringQQ := "402838678"

	// 数字字符串转换为整型
	numberQQ, _ := strconv.Atoi(stringQQ)
	// 通过反射获取到当前变量类型 => int
	fmt.Println(reflect.TypeOf(numberQQ))

	// 将整型转换为数字字符串
	stringQQ = strconv.Itoa(numberQQ)
	// 通过反射获取到当前变量类型 => string
	fmt.Println(reflect.TypeOf(stringQQ))
}
