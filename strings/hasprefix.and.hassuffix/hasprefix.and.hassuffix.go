package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := "中华人民共和国今天成立了"

	// 判断字符串是否包含特定的子串前缀
	isHasPrefix := strings.HasPrefix(s, "中华")
	// result is => true
	fmt.Println(isHasPrefix)
	// result is => false
	fmt.Println(strings.HasPrefix(s, "中国海军"))

	// 判断字符串是否包含特定的子串后缀
	isHasSuffix := strings.HasSuffix(s, "今天成立了")
	// result is => true
	fmt.Println(isHasSuffix)
	// result is => false
	fmt.Println(strings.HasSuffix(s, "不断壮大"))
}
