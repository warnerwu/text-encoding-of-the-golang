package main

import (
	"strings"
	"fmt"
)

func main() {
	// 字符串定义
	s := " 中华人  民共和国\r\n          今天成立了 "

	// 去除字符串中的前后空格
	newString := strings.Trim(s, " ")
	// result is:
	// '中华人  民共和国
	//           今天成立了'
	fmt.Println("'" + newString + "'")

	// 去除字符串中的空格
	newString = strings.Replace(newString, " ", "", -1)
	// result is:
	// '中华人民共和国
	// 今天成立了'
	fmt.Println("'" + newString + "'")

	// 去除字符串中的「回车换行」
	newString = strings.Replace(newString, "\r\n", "", -1)
	// result is:
	// '中华人民共和国今天成立了'
	fmt.Println("'" + newString + "'")
}