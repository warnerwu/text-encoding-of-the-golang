package main

import (
	"fmt"
	"strings"
)

func main() {
	// 字符串定义
	s := "中华人民共和国成立了, 三军皆在成长！"

	// 字符串替换
	newString := strings.Replace(strings.Replace(s, "了", "啦", -1), "成长", "快速强大", -1)
	// 中华人民共和国成立啦, 三军皆在快速强大！
	fmt.Println(newString)
}
