package main

import (
	"fmt"
	"strings"
)

func main() {
	// 字符串定义
	s := "中华人民共和国今天成立了"

	// 判断字符串是否包含	=> true false
	fmt.Println(strings.Contains(s, "今天"), strings.Contains(s, "毛主席"))

	// 获取字符串中子串的索引	=> 6 [UTF-8编码一个汉字占3个字节哦]
	fmt.Println(strings.Index(s, "人民"))
	// 获取字符串中子串的索引 	=> 4 [UTF-8编码单字节字母占一个字节]
	fmt.Println(strings.Index("i love u", "ve"))
}
